from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,    
)

from .forms import FormCliente, FormFactura, FormMesa, FormMesero, FormProducto, FormOrden
from .models import Cliente, Mesero, Mesa, Producto, Factura, Orden


from rest_framework.views import APIView
from rest_framework.response import Response
from appweb.serializer import *
from rest_framework import viewsets, permissions

class ClienteViewSet(viewsets.ModelViewSet):
    queryset = Cliente.objects.all()
    serializer_class = ClienteSerializer
    #permission_classes = [permissions.IsAuthenticated]

class MeseroViewSet(viewsets.ModelViewSet):
    queryset = Mesero.objects.all()
    serializer_class = MeseroSerializer

class MesaViewSet(viewsets.ModelViewSet):
    queryset = Mesa.objects.all()
    serializer_class = MesaSerializer

class ProductoViewSet(viewsets.ModelViewSet):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer

class FacturaViewSet(viewsets.ModelViewSet):
    queryset = Factura.objects.all()
    serializer_class = FacturaSerializer

class OrdenViewSet(viewsets.ModelViewSet):
    queryset = Orden.objects.all()
    serializer_class = OrdenSerializer
    

def index(request):
    return render(request, 'index.html')

class ClienteListView(ListView):
    model = Cliente
    template_name = 'cliente/list_clients.html'
    context_object_name = 'clients'

class ClienteDetailView(DetailView):
    model = Cliente
    template_name = 'cliente/detail_clients.html'
    context_object_name = 'clients'

class ClienteCreateView(CreateView):
    model=Cliente
    template_name = 'cliente/create_cliente.html'
    form_class = FormCliente
    success_url = reverse_lazy('listar_clientes')

class ClienteUpdateView(UpdateView):
    model = Cliente
    template_name = 'cliente/create_cliente.html'
    fields=['nombre','apellido', 'observaciones']
    success_url = reverse_lazy('listar_clientes')

class ClienteDeleteView(DeleteView):
    model = Cliente
    template_name='cliente/delete_cliente.html'
    success_url = reverse_lazy('listar_clientes')

class MeseroListView(ListView):
    model = Mesero
    template_name = 'meseros/list_waiters.html'
    context_object_name = 'waiters'

class MeseroDetailView(DetailView):
    model = Mesero
    template_name = 'meseros/detail_waiters.html'
    context_object_name = 'waiters'

class MeseroCreateView(CreateView):
    model=Mesero
    template_name = 'meseros/create_waiter.html'
    form_class = FormMesero
    success_url = reverse_lazy('listar_meseros')
    
class MeseroUpdateView(UpdateView):
    model = Mesero
    template_name = 'meseros/create_waiter.html'
    fields=['name','apellido_1','apellido_2']
    success_url = reverse_lazy('listar_meseros')

class MeseroDeleteView(DeleteView):
    model = Mesero
    template_name='meseros/delete_waiter.html'
    success_url = reverse_lazy('listar_meseros')

class MesaListView(ListView):
    model = Mesa
    template_name = 'tables/list_tables.html'
    context_object_name = 'tables'

class MesaDetailView(DetailView):
    model = Mesa
    template_name = 'tables/detail_tables.html'
    context_object_name = 'tables'

class MesaCreateView(CreateView):
    model=Mesa
    template_name = 'tables/create_table.html'
    form_class = FormMesa
    success_url = reverse_lazy('listar_mesas')

class MesaUpdateView(UpdateView):
    model = Mesa
    template_name = 'tables/create_table.html'
    fields=['num_comensales','ubicacion']
    success_url = reverse_lazy('listar_mesas')

class MesaDeleteView(DeleteView):
    model = Mesa
    template_name='tables/delete_table.html'
    success_url = reverse_lazy('listar_mesas')
    

class ProductoListView(ListView):
    model = Producto
    template_name = 'products/list_products.html'
    context_object_name = 'products'


class ProductoDetailView(DetailView):
    model = Producto
    template_name = 'products/detail_products.html'
    context_object_name = 'products'

class ProductoCreateView(CreateView):
    model=Producto
    template_name = 'products/create_product.html'
    form_class = FormProducto
    success_url = reverse_lazy('listar_productos')

class ProductoUpdateView(UpdateView):
    model = Producto
    template_name = 'products/create_product.html'
    fields=['nombre','descripcion', 'importe']
    success_url = reverse_lazy('listar_productos')

class ProductoDeleteView(DeleteView):
    model = Producto
    template_name='products/delete_product.html'
    success_url = reverse_lazy('listar_productos')
    

class FacturaListview(ListView):
    model = Factura
    template_name = 'facturas/list_facturas.html'
    context_object_name = 'facturas'


class FacturaDetailView(DetailView):
    model = Factura
    template_name = 'facturas/detail_facturas.html'
    context_object_name = 'facturas'


class FacturaCreateView(CreateView):
    model=Factura
    template_name = 'facturas/create_factura.html'
    form_class = FormFactura
    success_url = reverse_lazy('listar_facturas')

class FacturaUpdateView(UpdateView):
    model = Factura
    template_name = 'facturas/create_factura.html'
    fields = ['fecha_factura', 'cliente', 'mesa', 'mesero']
    success_url = reverse_lazy('listar_facturas')

class FacturaDeleteView(DeleteView):
    model = Factura
    template_name='facturas/delete_factura.html'
    success_url = reverse_lazy('listar_facturas')


class OrdenListView(ListView):
    model = Orden
    template_name = 'orders/list_orders.html'
    context_object_name = 'orders'

class OrdenDetailView(DetailView):
    model = Orden
    template_name = 'orders/detail_orders.html'
    context_object_name = 'orders'

class OrdenCreateView(CreateView):
    model=Orden
    template_name = 'orders/create_order.html'
    form_class = FormOrden
    success_url = reverse_lazy('listar_ordenes')

class OrdenUpdateView(UpdateView):
    model = Orden
    template_name = 'orders/create_order.html'
    fields = ['factura', 'producto', 'cantidad', 'total_item']
    success_url = reverse_lazy('listar_ordenes')

class OrdenDeleteView(DeleteView):
    model = Orden
    template_name='orders/delete_order.html'
    success_url = reverse_lazy('listar_ordenes')
