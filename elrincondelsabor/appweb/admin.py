from django.contrib import admin
from django.db.models import Sum

from appweb.models import Cliente, Mesero, Mesa, Producto, Factura, Orden 

class ClienteAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'apellido', 'observaciones')

class MeseroAdmin(admin.ModelAdmin):
    list_display = ('name', 'apellido_1', 'apellido_2')

class MesaAdmin(admin.ModelAdmin):
    list_display = ('num_comensales', 'ubicacion')

class ProductoAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'descripcion', 'importe')

class OrdenAdmin(admin.ModelAdmin):
    list_display = ('factura', 'producto', 'cantidad', 'itemtotal')

    def itemtotal(self, obj):
        return obj.cantidad * obj.producto.importe

    itemtotal.short_description = 'Total Item'
    itemtotal.allow_tags = True


class FacturaAdmin(admin.ModelAdmin):
    list_display = ('fecha_factura', 'cliente','mesero', 'mesa', 'total')

    def total(self,obj):
        tot=Orden.objects.filter(factura=obj.pk).aggregate(Sum('total_item'))
        return tot['total_item__sum']
        
    total.short_description = 'Total Factura'
    total.allow_tags = True

admin.site.register(Cliente, ClienteAdmin)
admin.site.register(Mesero, MeseroAdmin)
admin.site.register(Mesa, MesaAdmin)
admin.site.register(Producto, ProductoAdmin)
admin.site.register(Factura, FacturaAdmin)
admin.site.register(Orden, OrdenAdmin)
