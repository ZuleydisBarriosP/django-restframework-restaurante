from django.db import models
from django.db.models import Sum

class Cliente(models.Model):
    nombre = models.CharField(max_length=45)
    apellido = models.CharField(max_length=45)
    observaciones = models.CharField(max_length=45, blank=True, null=True)

    def __str__(self):
        return self.nombre

class Mesero(models.Model):
    name = models.CharField(max_length=45)
    apellido_1 = models.CharField(max_length=45)
    apellido_2 = models.CharField(max_length=45)

    def __str__(self):
        return self.name

class Mesa(models.Model):
    num_comensales = models.PositiveSmallIntegerField()
    ubicacion = models.CharField(max_length=45)

    def __str__(self):
        return self.ubicacion


class Producto(models.Model):
    nombre = models.CharField(max_length=45)
    descripcion = models.CharField(max_length=45)
    importe = models.FloatField()

    def __str__(self):
        return self.nombre


class Factura(models.Model):
    fecha_factura = models.DateField()
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    mesero = models.ForeignKey(Mesero, on_delete=models.CASCADE)
    mesa = models.ForeignKey(Mesa, on_delete=models.CASCADE)
    total_factura = models.FloatField(default=0)

    """ def get_total_orden(self):
        total = Orden.objects.filter(factura=self).aggregate(Sum('total_item'))
        return total['total_item__sum'] """
    
    def __str__(self):
        return str(self.id)


class Orden(models.Model):
    factura = models.ForeignKey(Factura, on_delete=models.CASCADE)
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    cantidad = models.PositiveSmallIntegerField()
    total_item = models.FloatField(default=0)

    """ def get_total_orden(self):
        self.total_item = self.cantidad * self.producto.importe
        self.save() """
    
    def __str__(self):
        return str(self.id)
    
    




