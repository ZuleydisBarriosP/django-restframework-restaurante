from .models import Cliente, Mesero, Mesa, Producto, Factura, Orden

from rest_framework import serializers


class ClienteSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Cliente
        fields = ['url', 'nombre', 'apellido', 'observaciones']

class MeseroSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Mesero
        fields = ['url', 'name', 'apellido_1', 'apellido_2']

class MesaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Mesa
        fields = ['url','id', 'num_comensales', 'ubicacion']

class ProductoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = ['id', 'nombre', 'descripcion', 'importe']

class FacturaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Factura
        fields = ['id', 'fecha_factura', 'cliente', 'mesero', 'mesa', 'total_factura']

class OrdenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Orden
        fields = ['id', 'factura', 'producto', 'cantidad', 'total_item']



