from django.urls import path

from rest_framework import routers

from appweb.views import *

router = routers.DefaultRouter()
router.register(r'clientes', ClienteViewSet)
router.register(r'meseros', MeseroViewSet)
router.register(r'mesas', MesaViewSet)
router.register(r'productos', ProductoViewSet)
router.register(r'facturas', FacturaViewSet)
router.register(r'ordenes', OrdenViewSet)



urlpatterns = [
    path('', index, name='index'),

    path('listar_clientes/', ClienteListView.as_view(), name='listar_clientes'),
    path('detalle_cliente/<int:pk>/', ClienteDetailView.as_view(), name='detalle_cliente'),
    path('crear_cliente/', ClienteCreateView.as_view(), name='crear_cliente'),
    path('actualizar_cliente/<int:pk>/', ClienteUpdateView.as_view(), name='actualizar_cliente'),
    path('eliminar_cliente/<int:pk>/', ClienteDeleteView.as_view(), name='eliminar_cliente'),

    path('listar_meseros/', MeseroListView.as_view(), name='listar_meseros'),
    path('detalle_mesero/<int:pk>/', MeseroDetailView.as_view(), name='detalle_mesero'),
    path('crear_mesero/', MeseroCreateView.as_view(), name='crear_mesero'),
    path('actualizar_mesero/<int:pk>/', MeseroUpdateView.as_view(), name='actualizar_mesero'),
    path('eliminar_mesero/<int:pk>/', MeseroDeleteView.as_view(), name='eliminar_mesero'),
    
    path('listar_mesas/', MesaListView.as_view(), name='listar_mesas'),
    path('detalle_mesa/<int:pk>/', MesaDetailView.as_view(), name='detalle_mesa'),
    path('crear_mesa/', MesaCreateView.as_view(), name='crear_mesa'),
    path('actualizar_mesa/<int:pk>/', MesaUpdateView.as_view(), name='actualizar_mesa'),
    path('eliminar_mesa/<int:pk>/', MesaDeleteView.as_view(), name='eliminar_mesa'),

    path('listar_productos/', ProductoListView.as_view(), name='listar_productos'),
    path('detalle_producto/<int:pk>/', ProductoDetailView.as_view(), name='detalle_producto'),
    path('crear_producto/', ProductoCreateView.as_view(), name='crear_producto'),
    path('actualizar_producto/<int:pk>/', ProductoUpdateView.as_view(), name='actualizar_producto'),
    path('eliminar_producto/<int:pk>/', ProductoDeleteView.as_view(), name='eliminar_producto'),
    

    path('listar_facturas/', FacturaListview.as_view(), name='listar_facturas'),
    path('detalle_factura/<int:pk>/', FacturaDetailView.as_view(), name='detalle_factura'),
    path('crear_factura/', FacturaCreateView.as_view(), name='crear_factura'),
    path('actualizar_factura/<int:pk>/', FacturaUpdateView.as_view(), name='actualizar_factura'),
    path('eliminar_factura/<int:pk>/', FacturaDeleteView.as_view(), name='eliminar_factura'),

    path('listar_ordenes/', OrdenListView.as_view(), name='listar_ordenes'),
    path('detalle_orden/<int:pk>/', OrdenDetailView.as_view(), name='detalle_orden'),
    path('crear_orden/', OrdenCreateView.as_view(), name='crear_orden'),
    path('actualizar_orden/<int:pk>/', OrdenUpdateView.as_view(), name='actualizar_orden'),
    path('eliminar_orden/<int:pk>/', OrdenDeleteView.as_view(), name='eliminar_orden'),
    
    
]
