from django import forms
from django.forms import ModelForm

from appweb.models import Cliente, Mesero, Mesa, Producto, Factura, Orden


class FormCliente(forms.ModelForm):
    class Meta:
        model = Cliente
        fields = (
            'nombre',
            'apellido',
            'observaciones',
        )


class FormMesero(forms.ModelForm):
    class Meta:
        model = Mesero
        fields = (
            'name',
            'apellido_1',
            'apellido_2',
        )


class FormMesa(forms.ModelForm):
    class Meta:
        model = Mesa
        fields = (
            'num_comensales',
            'ubicacion',
        )


class FormProducto(forms.ModelForm):
    class Meta:
        model = Producto
        fields = (
            'nombre',
            'descripcion',
            'importe',
        )


class FormFactura(forms.ModelForm):
    fecha_factura = forms.DateField(widget=forms.SelectDateWidget)
    cliente = forms.ModelChoiceField(queryset=Cliente.objects.all())
    mesero = forms.ModelChoiceField(queryset=Mesero.objects.all())
    mesa = forms.ModelChoiceField(queryset=Mesa.objects.all())

    class Meta:
        model = Factura
        fields = (
            'fecha_factura',
            'cliente',
            'mesero',
            'mesa',
        )


class FormOrden(forms.ModelForm):
    factura = forms.ModelChoiceField(queryset=Factura.objects.all())
    producto = forms.ModelChoiceField(queryset=Producto.objects.all())

    class Meta:
        model = Orden
        fields = (
            'factura',
            'producto',
            'cantidad',
            'total_item'
        )
